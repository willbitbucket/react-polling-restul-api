import React from 'react';
import RaceDetailV2 from '../../components/RaceDetailV2';

import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';


let r = {
  "id": "r0",
  "created_on": "2017-12-04T15:05:30.947092617+11:00",
  "type": "Thoroughbred",
  "closeDateTime": "2017-12-04T15:06:50.947092674+11:00",
  "competitors": [
    {
      "id": "c83",
      "position": 70
    },
    {
      "id": "c84",
      "position": 47
    },
    {
      "id": "c10",
      "position": 65
    },
    {
      "id": "c62",
      "position": 29
    }
  ]
}

describe('RaceDetailV2 unit test', function() {

  it('should exist', function() {
    const data = {pending: false, fulfilled:true, reason: null, rejected:false, value:r, match:{}}

    let c = TestUtils.renderIntoDocument(<RaceDetailV2 raceFetch={data}/>);
      expect(TestUtils.isCompositeComponent(c)).toBeTruthy();
    });

  it('should build the RaceDetailV2 passed as prop', function() {
    const data = {pending: false, fulfilled:true, reason: null, rejected:false, value:r, match:{}}

    let c = TestUtils.renderIntoDocument(<RaceDetailV2 raceFetch={data}/>);

    let buttons = TestUtils.scryRenderedDOMComponentsWithClass(c, 'position');
    expect(buttons.length).toEqual(4);
  });


});
