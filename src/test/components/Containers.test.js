import React from 'react';
import Containers from '../../components/Containers';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

let r = {
  id: '1',
  type:'Pilsner',
  minT: 4,
  maxT: 6,
  temperature: 3
}

describe('Containers unit test', function() {
  it('should exist', function() {
      const data = {loading:false, error: null, containers:[], subscribeToMore: () => {}}

      let c = TestUtils.renderIntoDocument(<Containers data={data}/>);
      expect(TestUtils.isCompositeComponent(c)).toBeTruthy();
    });

  it('should build the container list from an array of objects passed as prop', function() {
    const data = {loading:false, error: null, containers:[r], subscribeToMore: () => {}}

    let c = TestUtils.renderIntoDocument(<Containers data={data}/>);

    let containers = TestUtils.scryRenderedDOMComponentsWithClass(c, 'message');
    expect(containers.length).toEqual(1);
    expect(containers[0].textContent).toEqual(`Container ${r.id} (${r.type}), requires:[${r.minT}, ${r.maxT}], measured:${r.temperature} °C`);
  });

});
