import React from 'react';
import RacesV2 from '../../components/RacesV2';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

let r = {
  "id": "r0",
  "created_on": "2017-12-04T15:05:30.947092617+11:00",
  "type": "Thoroughbred",
  "closeDateTime": "2017-12-04T15:06:50.947092674+11:00",
  "competitors": [
    {
      "id": "c83",
      "position": 70
    },
    {
      "id": "c84",
      "position": 47
    },
    {
      "id": "c10",
      "position": 65
    },
    {
      "id": "c62",
      "position": 29
    }
  ]
}

describe('RacesV2 unit test', function() {
  it('should exist', function() {
      const data = {pending: false, fulfilled:true, reason: null, rejected:false, value:[], match:{}}

      let c = TestUtils.renderIntoDocument(<RacesV2 racesFetch={data}/>);
      expect(TestUtils.isCompositeComponent(c)).toBeTruthy();
    });

  it('should build the races list from an array of objects passed as prop', function() {
    const data = {pending: false, fulfilled:true, reason: null, rejected:false, value:[r], match:{}}

    let c = TestUtils.renderIntoDocument(<RacesV2 racesFetch={data}/>);

    let containers = TestUtils.scryRenderedDOMComponentsWithClass(c, 'message');
    expect(containers.length).toEqual(1);
    expect(containers[0].textContent).toEqual("r0-Thoroughbred, close in 0.0 seconds");
  });

});
