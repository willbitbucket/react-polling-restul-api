import * as service from '../../service/container';
import { expect } from "chai";


const c1 = {
  id: '1',
  type:'A',
  minT: 4,
  maxT: 6,
  temperature: 7
},
c2 = {
  id: '2',
  type:'B',
  minT: 4,
  maxT: 6,
  temperature: 3
};
const containers = [c1, c2]

describe('container service unit test', function() {
    describe('filterBelowRange', function () {
        it("returns below range", function (done) {
            expect(JSON.stringify(service.filterBelowRange(containers))).to.equal(JSON.stringify([c2]));
            done()
        });
    })

    describe('filterAboveRange', function () {
        it("returns below range", function (done) {
            expect(JSON.stringify(service.filterAboveRange(containers))).to.equal(JSON.stringify([c1]));
            done()
        });
    })

    afterEach(()=>{
  
    })

})
