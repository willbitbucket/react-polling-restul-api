import React, { Component } from 'react';

import CountDown from './CountDown';

class RacesV2 extends Component {

  render() {
    const { racesFetch: {pending, rejected, fulfilled,reason, value }, match } = this.props;


    if (pending) {
      return (<div  className="channelName">Loading...</div>);
    }
    if (rejected) {
      return (<div  className="channelName">{reason}</div>);
    }
    const races = value;
    if(races === null || races === undefined){
      return (<div  className="channelName">No races open now...</div>)
    }
    return (
      <div>
        <div className="messagesList">
        {
            races.map( (r, i) =>
             {
              return (
                <div className="message" key={i}>
                <p>
                    <a className="title" href={ `race/${r.id}`}>
                     {r.id}-{r.type}, close in <CountDown end={new Date(r.closeDateTime)} />
                   </a>

                </p>
                </div>
               )}

            )
        }
        </div>
      </div>);
  }
}



export default RacesV2