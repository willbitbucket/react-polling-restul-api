import React from 'react';

import { connect } from 'react-refetch'
import RaceDetailV2 from './RaceDetailV2';

export default connect(props => ({
  raceFetch: {url: `http://localhost:8000/race/${props.match.params.id}`,
    refreshInterval: 1000}
}))(RaceDetailV2)