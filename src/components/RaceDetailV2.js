import React, { Component } from 'react';
import CountDown from './CountDown';
class RaceDetailV2 extends Component {

  render() {
    const {raceFetch: {pending, rejected, fulfilled, reason, value}, match} = this.props;

    if (pending) {
      return (<div className="channelName">Loading...</div>);
    }
    if (rejected) {
      return (<div className="channelName">{reason}</div>);
    }

    let race = value;
    if (!race) {
      return (<div className="channelName">Not found...</div>);
    }

    return (
        <div>
          <div className="channelName">
            <div className="title">Race{race.id}, close on {new Date(race.closeDateTime).toLocaleTimeString()}, in {new Date(race.closeDateTime) > new Date() ? <CountDown end={new Date(race.closeDateTime)} />:'closed'}</div>
            <p></p>
            {
              race.competitors.map(ch =>
                  (<div>
                    <label className="position">{ch.position}</label>&nbsp;
                    <button className="bet" type="submit"> {new Date(race.closeDateTime) > new Date() ? 'Bet now' : 'Closed'}</button>
                  </div>)
              )}
          </div>
        </div>);
  }
}



export default RaceDetailV2
