import React, { Component } from 'react';
import RacesV2 from './RacesV2';


import { connect } from 'react-refetch'


export default connect(props => ({
    racesFetch: {url: `http://localhost:8000/race`,
        refreshInterval: 1000}
}))(RacesV2)