import React, { Component } from 'react';
import {
  BrowserRouter,
  Link,
  Route,
  Switch,
} from 'react-router-dom';

import './App.css';
import NotFound from './components/NotFound';
import RaceDetailV2Wrapper from './components/RaceDetailV2Wrapper';
import RacesV2Wrapper from './components/RacesV2Wrapper';


import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';


class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <div className="App">
            <Link to="/" className="navbar">Enjoy the fun</Link>
            <Switch>
              <Route exact path="/" component={RacesV2Wrapper}/>
              <Route path="/races/" component={RacesV2Wrapper}/>
              <Route path="/race/:id" component={RaceDetailV2Wrapper}/>
              <Route component={ NotFound }/>
            </Switch>
          </div>
        </BrowserRouter>

    );
  }
}

export default App;
